var app = new Vue({
    el:"#vue",
    data:{
        map:null,
        initLontitude:[45.760463, 4.863472],
        initZoom:13,
        fires:[],
        fireStation:[],
        notifications:[], //{type="new|upd|del",intensity:"" ,idFire:"",time}
        sensors:[],
        filterFire:false,
        filterTruck:false,
        timestamp: 0
    },
    methods:{
        //====Fire on MAP=======
        drawFire:async function(){
            var numberpoint = [6,10]
            var i ,j 
            this.removeFire()
            var geojsonMarkerOptions = {
                radius: 8,
                fillColor: "#FF0000",
                color: "#000",
                weight: 1,
                opacity: 1,
                fillOpacity: 0.8
            };

            
            this.sensors.forEach(s=>{
                var f  = this.fires.find(f=>{
                    return f.idSensor==s.id
                })
              
                geojsonMarkerOptions.fillColor= this.getColorFire(f?f.intensity:0)
                
                s.marker = L.circleMarker([s.latitude,s.longitude], geojsonMarkerOptions).addTo(this.map).bindPopup(`X:${s.longitude} Y:${s.latitude}<br>Intensity:${f?f.intensity:0}`)
            })
        },
        getFires:async function(){
            this.fires = await uhttp('get','db/fire')
        },
        getColorFire(intensity){
            if(!intensity)
                return "#FFF"
            
            return "rgb(255,"+(10-intensity)*25+",0)"
        },
        removeFire(){
            this.sensors.forEach((s)=>{
                if(s.marker)
                    s.marker.remove()
            })
        },
        //====truck on map
        getViewTruck:async function(){
            this.removeTruck()
            var res = await uhttp('GET','db/truckinfo')
            this.fireStation = _.groupBy(res,"fireStationID")            
        },
        drawFireStation:function(){
            var firestation = L.icon({
                iconUrl: 'img/firestation.png',
                //shadowUrl: 'leaf-shadow.png',
            
                iconSize:     [30, 30], // size of the icon
                //shadowSize:   [50, 64], // size of the shadow
                iconAnchor:   [15, 15], // point of the icon which will correspond to marker's location
                //shadowAnchor: [4, 62],  // the same for the shadow
                popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
            });
            _.forEach(this.fireStation,fs=>{                
                L.marker([fs[0].latitude, fs[0].longitude], {icon: firestation}).addTo(this.map).bindPopup(fs[0].name);
            })
            
        },
        drawFireTruck:function(){
            var firetruck = L.icon({
                iconUrl: 'img/firetruck.png',
                //shadowUrl: 'leaf-shadow.png',
            
                iconSize:     [30, 15], // size of the icon
                //shadowSize:   [50, 64], // size of the shadow
                iconAnchor:   [15, 6], // point of the icon which will correspond to marker's location
                //shadowAnchor: [4, 62],  // the same for the shadow
                popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
            });
            allCoordUse=[]
            _.forEach(this.fireStation,fs=>{
                fs.forEach(async tr=>{
                    coord=null
                    if(tr.state==0){
                        var coord =[tr.latitude,tr.longitude]
                    }
                    if(tr.state==2){
                        try{
                            var nav = await uhttp("GET",'getnavigation/'+tr.fireTruckID)
                            if (nav && nav.length)
                                this.$set(tr.marker = L.polyline(nav, {color: 'blue'}).addTo(this.map).bindPopup("Camion "+tr.fireTruckID));

                        }catch(e){
                            console.log("chemin inconnue");
                            
                        }
                        // if(tr.idFire){
                        //     var f = this.fires.find(f=>f.id==tr.idFire)
                        //     var s =this.sensors.find(s=>s.id==f.idSensor)
                        //     var latlngs=[
                        //         [tr.latitude,tr.longitude],
                        //         [s.latitude,s.longitude],
                        //     ]
                        //     tr.marker = L.polyline(latlngs, {color: 'blue'}).addTo(this.map).bindPopup("Camion "+tr.fireTruckID);
                        // }
                    }
                    if(tr.state==1){
                        var f  = this.fires.find(f=>f.id==tr.idFire)
                        if(!f){
                            console.warn("fire unknow");
                            
                        }else{

                            var s =this.sensors.find(s=>s.id==f.idSensor)
                            var coord = [s.latitude,s.longitude]
                        }
                    }
                    if(coord){
                        var t = allCoordUse.find(c=>c.coord[0]==coord[0]&&c.coord[1]==coord[1])
                        var i = t?t.i:0
                        var decalageX = i==0||i==3||i==5?-1 : i==1||i==6?0:1
                        var decalageY = i<3?-1:i<5?0:1
                        
                        x = coord[0] + decalageX*0.002
                        y = coord[1] + decalageY*0.002
                        tr.marker = L.marker([x,y], {icon: firetruck}).addTo(this.map).bindPopup("Camion "+tr.fireTruckID);
                        if(t){
                            t.i = (t.i+1)%8
                        }else{
                            allCoordUse.push({coord:coord,i:1})
                        }
                    }
                })

            })
        },
        removeTruck:function(){
            _.forEach(this.fireStation,fs=>{
                _.forEach(fs,tr=>{
                    if(tr.marker){
                        
                        tr.marker.remove()
                    }
                })
            })
        },
        //======event======
        hoverTruckInfo:function(id,show){
            var truck = this.getTruck(id)
            if(truck.marker){
                
                if(show)
                    truck.marker.openPopup()
                else
                    truck.marker.closePopup()
            }
        },
        hoverFireInfo:function(id,show){
            console.log(id,show);
            
            var sensor = this.sensors.find(s=>s.id==id)
            if(sensor.marker){
                
                if(show)
                    sensor.marker.openPopup()
                else
                    sensor.marker.closePopup()
            }
        },
        //======notification===
        getNotification:async function(){
            var thisTimeStamp = this.timestamp
            this.timestamp = new Date().getTime()
            var newNotif = await uhttp("GET",'getNotifications/'+thisTimeStamp)
            if(newNotif.length){
                new Notification("Mise a jour")
                this.notifications= this.notifications.concat(newNotif)
                this.notifications.sort((a,b)=>{
                    return a.time>b.time?-1:1
                })
            }
        },
        //usefultool
        getTruck:function(id){
            return _.reduce(this.fireStation,(s,f)=>{
                var r= f.find(t=>t.fireTruckID==id)

                return r?r:s
            },null)
        },
        getLibNotif:function(id){
            var r = {"NEW":"Nouvel incendie",
            "UPDATE":"Mise a jour",
            "DELETE":"Fin d'incendie",
            }
            return r[id]
        },
        
        refresh :async function(){
            await this.getFires()
            if(!this.filterFire)
                this.drawFire()
            await this.getViewTruck()
            this.drawFireStation()
            if(!this.filterTruck)
                this.drawFireTruck()
            this.getNotification()
        }
    },
    watch:{
        filterFire:function(){
            if(this.filterFire)
                this.removeFire()
            else
                this.drawFire()
        },
        filterTruck:function(){
            if(this.filterTruck)
                this.removeTruck()
            else
                this.drawFireTruck()
        }
    },
    mounted:async function(){
        this.map = L.map('map').setView(this.initLontitude, this.initZoom);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
        this.sensors = await uhttp("GET","db/fireSensor")
        
        // await this.getFires()
        // this.drawFire()
        this.refresh()
        setInterval( ()=>{
            app.refresh()
        },3000)
    }
})
function enableNotif(){
    Notification.requestPermission( function(status) {
        console.log(status); // les notifications ne seront affichées que si "autorisées"
        var n = new Notification("title", {body: "notification body"}); // this also shows the notification
    });
}