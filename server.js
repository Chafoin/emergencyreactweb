const express = require('express')

const db = require('./module/query')
const fs = require('fs')
const path = require('path')
const app = express()
const port = 3001



app.use(express.static('public'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}));

app.get('/', (req, res) => {
    res.sendFile(path.resolve('index.html'));
  })
app.get('/db/:table', db.getAll)
app.get('/db/:table/:id', db.getOne)
app.post('/db/:table', db.insert)
app.put('/db/:table/:id', db.update)

app.post('/fireInformation', db.fireFormatController)
app.get('/getNotifications/:time', db.getNotif)

//permet de preciser un trajet a un camion
app.post("/navigation/:idFireTruck",db.setNavigation)
app.get("/getnavigation/:idFireTruck",db.getNavigation)
app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})