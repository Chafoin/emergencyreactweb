const Pool = require('pg').Pool
const pool = new Pool({
  user: 'tp',
  host: 'localhost',
  database: 'emergencyReact',
  password: 'tp',
  port: 5432,
})
var notifications = []
function showError(response,e){
    response.sendStatus(500)
    console.log("error",e)
}

var allNavigations=[]

const getAll = (request, response) => {
  const table = request.params.table
  pool.query('SELECT * FROM public."'+request.params.table+'"',(error, results) => {
    //response.setRequestHeader("Content-Type","application/json");
    
    if(!results){
      response.status(200).json([])
      return
    }
    if (error) {
      showError(response,error)
    }
    response.status(200).json(results.rows)
  })
}

const getOne = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM public."'+request.params.table+'" WHERE id = $1', [id], (error, results) => {
    if (error) {
      showError(response,error)
    }
    response.status(200).json(results.rows)
  })
}

const insert = (request, response) => {
  var keys=""
  var values=""
  t = paramBodyInsertFormat(request.body)
  keys = t[0]
  values = t[1]
  var query='INSERT INTO public."'+request.params.table+'" ('+keys+') VALUES ('+values+') RETURNING id'
  
  pool.query(query, (error, results) => {
      if (error) {
          showError(response,error)
      }
      response.status(200).send(results.rows[0].id+"")
  })
}

const update = (request, response) => {
  var set= paramBodyUpdateFormat(request.body)
  pool.query(
      'UPDATE public."'+request.params.table+'" SET '+set+" where id = "+request.params.id,
      (error, results) => {
        if (error) {
            showError(response,error)
        }
      response.status(200).send(`ok`)
      }
  )
}
const fireFormatController = (request,response)=>{
  //recuperation du microtrolleur
  var newIncendie = request.body
  //Recepetion des incendies
  console.log(newIncendie);
  if(newIncendie.id==-1){
    console.log("clearall");
    
    pool.query('DELETE FROM intervention', (error, results) => {
      if (error) {
        console.log(error)
        return
      }
      pool.query('DELETE FROM fire', (error, results) => {
        if (error) {
          console.log(error)
          return
        }
        //notifications.push({type:"CLEAR",time:new Date().getTime()})
      })
    })
    response.status(200).send(`ok`)
    return; 
  }
  pool.query('SELECT * FROM fire where "dateFin" is null', (error, results) => {
    if (error) {
      showError(response,error)
      return
  }
    var onBaseIncendie = results.rows.find(i=>{
      return i.idSensor == newIncendie.id 
    })
    if(onBaseIncendie){
      //update
      if(newIncendie.i==0){
        pool.query("delete from fire CASCADE where id = "+onBaseIncendie.id,(error, results) => {
          if (error) {
              console.log(error)
              return
          }
          notifications.push({type:"DELETE",time:new Date().getTime(),id:newIncendie.id,i:newIncendie.i})
        })
      }else{
        pool.query("UPDATE fire SET intensity="+newIncendie.i+" where id = "+onBaseIncendie.id,(error, results) => {
          if (error) {
              showError(response,error)
              return
          }
          notifications.push({type:"UPDATE",time:new Date().getTime(),id:newIncendie.id,i:newIncendie.i})
        })
      }
      
    }else{
      //insert
      if(newIncendie.i){
        var query=`INSERT INTO Fire ("idSensor",intensity) VALUES (${newIncendie.id},${newIncendie.i}) RETURNING id`
        
        
        pool.query(query, (error, results) => {
          if (error) {
              showError(response,error)
          }
          notifications.push({type:"NEW",time:new Date().getTime(),id:newIncendie.id,i:newIncendie.i})
        })
      }
      
    }
    
    //cherche pour update /insert
    response.status(200).send(`ok`)
  })
}
    
const getNotif = (request,response)=>{
  var time = request.params.time
  var notifs = notifications.filter(n=>{
    return n.time>time
  })
  response.status(200).send(notifs)
}

const setNavigation = (request,response)=>{
  try{
    console.log(typeof request.body,request.body.length,request.body[0])
    allNavigations[request.params.idFireTruck] = request.body.map(n=>{
      var tmp = n[0]
      n[0] = n[1]
      n[1]= tmp
      return n
    })
    response.status(200).send("ok")
  }catch(e){
    console.log(e);
    response.status(500).send(e)
  }
}
const getNavigation = (request,response)=>{      
  if(allNavigations[request.params.idFireTruck]){
    response.status(200).send(allNavigations[request.params.idFireTruck])
  }
  else{
    response.status(500).send([])
  }
}

module.exports = {
  getAll,
  getOne,
  insert,
  update,
  fireFormatController,
  getNotif,
  setNavigation,
  getNavigation,
}
function paramBodyInsertFormat(body){
  var keys=""
  var values=""
  Object.keys(body).forEach((k,i)=>{
      var isNumber = typeof body[k] == "number"
      keys+=(k
          +(i+1!=Object.keys(body).length?",":""))
  
      values+=((isNumber?"":"'")
          +body[k]
          +(isNumber?"":"'")
          +(i+1!=Object.keys(body).length?",":""))
  })
  return [keys,values]
}
function paramBodyUpdateFormat(body){
  var s=""
  Object.keys(body).forEach((k,i)=>{
      if(k!='id'){
          var isNumber = typeof body[k] == "number"
          s+=(k
              +" = "
              +(isNumber?"":"'")
              +body[k]
              +(isNumber?"":"'")
              +(i+1!=Object.keys(body).length?",":"")
          )
      }
  })
  return s
}